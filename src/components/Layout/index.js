import React from "react"
import { LayoutWrapper } from "./styles"
import { Header } from "components"

const Layout = ({ children }) => {
  return (
    <LayoutWrapper>
      <Header />
      <main>{children}</main>
    </LayoutWrapper>
  )
}

export { Layout }
