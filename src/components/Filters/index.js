import React from "react"
import { CategoryFilter } from "../CategoryFilter"
import styled from "styled-components"

const FiltersWrapper = styled.div`
  border: 1px solid #efefef;
  padding: 8px;
  .cat-title {
    margin-bottom: 10px;
    display: block;
    font-size: 1.2em;
    color: #666;
  }
`

export function Filters({ collections }) {
  return (
    <FiltersWrapper>
      <div className="cat-title">Categories</div>
      {collections.map(coll => (
        <CategoryFilter collection={coll} key={coll.id} />
      ))}
    </FiltersWrapper>
  )
}
