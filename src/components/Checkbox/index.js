import React from "react"
import { FaCheck } from "react-icons/fa"
import styled from "styled-components"

const CheckboxWrapper = styled.span`
  background-color: ${props => props.checked ? "black" : "none" };
  border: 1px solid black;
  border-radius: 3px;
  width: 20px;
  height: 20px;
  display: inline-block;
  text-align: center;
  vertical-align: middle;
  line-height: 1;
  cursor: pointer;
`

export function Checkbox({ checked }) {
  return (
    <CheckboxWrapper checked={checked}>
      <FaCheck color="white" />
    </CheckboxWrapper>
  )
}
