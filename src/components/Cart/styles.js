import styled from "styled-components"
import { StyledLink } from "components/StyledLink"

export const CartWrapper = styled(StyledLink).attrs(() => ({
  to: "/cart",
}))`
  padding-left: 10px;
  .quantity {
    width: 20px;
    display: inline-block;
    text-align: center;
    margin: 0px 4px;
  }
  &:hover {
    text-decoration: underline;
  }
`
