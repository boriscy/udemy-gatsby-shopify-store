import React from "react"
import { CartWrapper } from "./styles"
import { FaShoppingCart } from "react-icons/fa"
import CartContext from "context/CartContext"

export function Cart() {
  const { checkout } = React.useContext(CartContext)

  let totalQuantity = 0
  if (checkout) {
    totalQuantity = checkout.lineItems.reduce((sum, li) => {
      return sum + li.quantity
    }, 0)
  }

  return (
    <CartWrapper>
      <FaShoppingCart size="1.5em" />
      <span className="bg-red-700 text-white rounded-full quantity">
        {totalQuantity}
      </span>
      <span className="text-gray-700">
        <span className="total font-semibold">
          {checkout?.totalPrice || "0.00"}
        </span>{" "}
        BOB
      </span>
    </CartWrapper>
  )
}
