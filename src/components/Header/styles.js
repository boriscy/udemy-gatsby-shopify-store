import styled from "styled-components"

export const HeaderWrapper = styled.header`
  height: 40px;
  display: flex;
  margin: 0px auto;
  box-sizing: border-box;
  flex-wrap: wrap;
`
