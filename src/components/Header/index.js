import React from "react"
import { Link } from "gatsby"
import { HeaderWrapper } from "./styles"
import { Cart, Search } from "components"

export function Header() {
  return (
    <HeaderWrapper>
      <Link to="/">Home</Link>
      <Search />
      <Cart />
    </HeaderWrapper>
  )
}
