import React from "react"
import { CollectionTile } from "components/CollectionTile"
import { OtherCollectionsWrapper } from "./styles"

export function HomepageCollectionsGrid({ collections }) {
  const saleCollection = collections.find(coll => coll.title === "SALE")
  const otherCollections = collections.filter(coll => coll.title !== "SALE")

  return (
    <div>
      {!!saleCollection && <CollectionTile collection={saleCollection} sale />}
      <OtherCollectionsWrapper>
        {otherCollections.map(coll => {
          return (
            <CollectionTile
              key={coll.id}
              collection={coll}
              destination={`/all-products?c=${encodeURIComponent(
                coll.shopifyId
              )}`}
            />
          )
        })}
      </OtherCollectionsWrapper>
    </div>
  )
}
