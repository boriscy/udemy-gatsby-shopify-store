import React from "react"
import { Link } from "gatsby"
import { ProductTileWrapper } from "./styles"
import Img from "gatsby-image"
import { Location } from "@reach/router"

export function ProductTile({ product, location }) {
  const img = product.images[0].localFile.childImageSharp.fluid

  return (
    <ProductTileWrapper>
      <Img fluid={img} />
      <div>
        <h3>{product.title}</h3>
        <p>{product.description}</p>
        <div className="price-wrapper">
          From{" "}
          <span className="price">
            {product.priceRange.minVariantPrice.amount}
          </span>{" "}
          BOB
        </div>
      </div>
      <Location>
        {({ location }) => {
          return (
            <Link
              to={`/products/${product.handle}`}
              state={{ prevPath: location.pathname }}
            >
              View
            </Link>
          )
        }}
      </Location>
    </ProductTileWrapper>
  )
}
