import styled from "styled-components"

export const ProductTileWrapper = styled.div`
  border: 1px solid #ddd;
  display: flex;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  overflow: hidden;
  flex-direction: column;
  > div {
    padding: 10px;
    flex-grow: 1;
    display: flex;
    flex-flow: column;
  }
  h3 {
    font-weight: bold;
    font-size: 1.3em;
  }
  p {
    color: #777;
    flex-grow: 1;
  }
  .price-wrapper {
    margin: 20px 0px 10px;
  }
  .price {
    font-weight: bold;
  }
  a {
    display: block;
    text-align: center;
    border: 1px solid #555;
    padding: 8px;
    font-weight: bold;
    text-decoration: none;
    align-self: flex-end;
    width: 100%;
    &:hover {
      background-color: #007eff;
      color: white;
    }
  }
`
