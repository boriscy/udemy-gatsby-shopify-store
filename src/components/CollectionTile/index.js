import React from "react"
import BackgroundImage from "gatsby-background-image"
import {
  CollectionTileWrapper,
  CollectionTileContent,
  Title,
  Description,
} from "./styles"
import { Link } from "gatsby"

export function CollectionTile({ collection, sale, destination }) {
  const img = collection.image
    ? collection.image.localFile.childImageSharp.fluid
    : ""

  return (
    <CollectionTileWrapper>
      <BackgroundImage fluid={img} />
      <CollectionTileContent>
        <div>
          <Title sale={sale}>{collection.title}</Title>
          <Description sale={sale}>{collection.description}</Description>
          <Link
            to={destination}
            className="py-2 px-8 border-2 border-gray-200 border-solid border-opacity-50 bg-opacity-20 bg-gray-50 hover:bg-opacity-10"
          >
            View
          </Link>
        </div>
      </CollectionTileContent>
    </CollectionTileWrapper>
  )
}
