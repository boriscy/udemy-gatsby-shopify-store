import React from "react"
import styled from "styled-components"
import { Checkbox } from "components"
import { navigate, useLocation } from "@reach/router"
import queryString from "query-string"

const CategoryFilterWrapper = styled.label`
  padding: 8px 5px;
  display: block;
  cursor: pointer;
`

export function CategoryFilter({ collection }) {
  const { search } = useLocation()
  const qs = queryString.parse(search)
  let collectionIds = qs.c?.split(",") || []
  const searchTerm = qs.s

  const onClick = () => {
    let navigateTo = "/all-products"

    if (collectionIds.includes(collection.shopifyId)) {
      const idx = collectionIds.findIndex(c => c === collection.shopifyId)
      collectionIds.splice(idx, 1)
    } else {
      collectionIds = [...collectionIds, collection.shopifyId]
    }

    let qs = ""
    if (collectionIds.length && !searchTerm) {
      qs = `?c=${encodeURIComponent(collectionIds.join(","))}`
    } else if (collectionIds.length && !!searchTerm) {
      qs = `?c=${encodeURIComponent(
        collectionIds.join(",")
      )}&s=${encodeURIComponent(searchTerm)}`
    }

    navigate(`${navigateTo}${qs}`)
  }

  return (
    <CategoryFilterWrapper onClick={onClick}>
      <Checkbox checked={collectionIds.includes(collection.shopifyId)} />
      <span className="ml-1">{collection.title}</span>
    </CategoryFilterWrapper>
  )
}
