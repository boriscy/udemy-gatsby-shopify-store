import React from "react"
import CartContext from "context/CartContext"
import { CartItem, CartHeader, CartFooter, Footer } from "./styles"
import { QuantityAdjuster } from "components/QuantityAdjuster"
import { FaTrash } from "react-icons/fa"
import { Link } from "gatsby"
import { navigate } from "@reach/router"

export function CartContent() {
  const { checkout, updateLineItem } = React.useContext(CartContext)
  const handleAdjustQuantity = ({ item, quantity }) => {
    updateLineItem({ variantId: item.variant.id, quantity })
  }

  return (
    <section>
      <h1>Your Cart</h1>
      {!!checkout?.lineItems?.length && (
        <CartHeader className="text-gray-600">
          <div></div>
          <div>Item</div>
          <div>Price</div>
          <div>Quantity</div>
          <div>Subtotal</div>
          <div></div>
        </CartHeader>
      )}

      <section>
        {checkout?.lineItems?.map(lineItem => {
          return (
            <CartItem key={lineItem.id}>
              <div>
                <img
                  src={lineItem.variant.image.src}
                  width="100"
                  alt="Product"
                />
              </div>
              <div>
                <div className="font-semibold">
                  <Link
                    to={
                      "/products/" +
                      lineItem.customAttributes[0].value +
                      "?variant=" +
                      lineItem.variant.id
                    }
                  >
                    {lineItem.title}
                  </Link>
                </div>
                <div className="text-gray-500">
                  {lineItem.variant.title === "Default Title"
                    ? ""
                    : lineItem.variant.title}
                </div>
              </div>
              <div>
                <span className="font-semibold">{lineItem.variant.price}</span>{" "}
                BOB
              </div>
              <div>
                <QuantityAdjuster
                  item={lineItem}
                  onAdjust={handleAdjustQuantity}
                />
              </div>
              <div>
                <span className="font-semibold">
                  {lineItem.quantity * lineItem.variant.price}
                </span>{" "}
                BOB
              </div>
              <div className="delete-row">
                <FaTrash
                  className="text-red-700"
                  onClick={() => {
                    handleAdjustQuantity({ item: lineItem, quantity: 0 })
                  }}
                  title="Remove Item"
                />
              </div>
            </CartItem>
          )
        })}
      </section>

      {!!checkout?.lineItems?.length && (
        <CartFooter>
          <div className="text-gray-600">TOTAL</div>
          <div>
            <span className="font-semibold">{checkout?.totalPrice}</span> BOB
          </div>
        </CartFooter>
      )}

      {!checkout?.lineItems?.length && (
        <div className="text-center">
          <h3 className="text-gray-500">
            You don't any products on your shopping cart.
          </h3>
          <div>
            <Link
              to="/all-products"
              className="text-blue-500 hover:text-blue-600 hover:underline text-2xl"
            >
              View all of our products
            </Link>
          </div>
        </div>
      )}
      <Footer>
        <div>
          <button
            className="border-gray-200 py-2 px-6 border-solid border-2 hover:bg-gray-800 hover:text-white"
            onClick={() => navigate(-1)}
          >
            Continue Shopping
          </button>
        </div>
        <div>
          {!!checkout?.webUrl && !!checkout?.lineItems?.length && (
            <button
              className="border-gray-200 py-2 px-6 border-solid border-2 hover:bg-gray-800 hover:text-white"
              onClick={() => {
                window.location.href = checkout.webUrl
              }}
            >
              Checkout
            </button>
          )}
        </div>
      </Footer>
    </section>
  )
}
