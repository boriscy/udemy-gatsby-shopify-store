import styled from "styled-components"

export const CartHeader = styled.div`
  grid-template-columns: 1fr 2fr 1fr 1fr 1fr 40px;
  border-bottom: 1px solid #ccc;
  display: grid;
  font-weight: bold;
  > div {
    padding: 8px;
  }
`

export const CartItem = styled.div`
  border-bottom: 1px solid #ccc;
  display: grid;
  grid-template-columns: 1fr 2fr 1fr 1fr 1fr 40px;

  > div {
    padding: 8px;
  }

  input[type="number"] {
    width: 100px;
  }

  .delete-row {
    svg {
      cursor: pointer;
    }
  }
`

export const CartFooter = styled.div`
  grid-template-columns: 5fr 1fr 40px;
  border-bottom: 1px solid #ccc;
  display: grid;
  > div {
    padding: 8px;
    &:first-child {
      text-align: right;
    }
  }
`

export const Footer = styled.footer`
  margin-top: 10px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  > div:last-child {
    text-align: right;
  }
`
