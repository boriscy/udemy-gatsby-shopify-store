import styled from "styled-components"

export const QuantityAdjusterWrapper = styled.div`
  input {
    margin: 0px 5px;
  }
  svg {
    cursor: pointer;
  }
`
