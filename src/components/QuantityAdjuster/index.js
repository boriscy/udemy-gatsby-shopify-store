import React from "react"
import { QuantityAdjusterWrapper } from "./styles"
import { FaPlus, FaMinus } from "react-icons/fa"

export function QuantityAdjuster({ item, onAdjust }) {
  const [quantity, setQuantity] = React.useState(item.quantity)
  const handleChange = ev => {
    const val = ev.currentTarget.value
    setQuantity(val)

    if (val !== "" && val.match(/^\d+/)) {
      if (+val > 0) {
        onAdjust({ item, quantity: +val })
      }
    }
  }

  const handleBlur = () => {
    if (quantity !== item.quantity) {
      setQuantity(item.quantity)
    }
  }

  const handleAdjust = newQuantity => {
    if (item.quantity + newQuantity < 1) return
    onAdjust({ item, quantity: quantity + newQuantity })
    setQuantity(quantity + newQuantity)
  }

  return (
    <QuantityAdjusterWrapper>
      <FaPlus onClick={() => handleAdjust(1)} />
      <input
        type="number"
        step="1"
        min="1"
        value={quantity}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <FaMinus onClick={() => handleAdjust(-1)} />
    </QuantityAdjusterWrapper>
  )
}
