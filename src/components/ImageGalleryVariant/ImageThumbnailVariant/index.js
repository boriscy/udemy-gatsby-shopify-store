import React from "react"
import Image from "gatsby-image"
import { ImageThumbnailWrapper } from "./styles"

export default function ImageThumbnailVariant({
  isActive,
  handleClick,
  variant,
}) {
  return (
    <ImageThumbnailWrapper
      onClick={() => {
        handleClick(variant)
      }}
      isActive={isActive}
    >
      <Image fluid={variant.image.localFile.childImageSharp.fluid} />
    </ImageThumbnailWrapper>
  )
}
