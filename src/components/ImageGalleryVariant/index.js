import React, { useState } from "react"
import Image from "gatsby-image"
import { ImageGalleryWrapper } from "./styles"
import ImageThumbnailVariant from "./ImageThumbnailVariant"

export function ImageGalleryVariant({ variants, variant, handleClick }) {
  const [activeImageThumbnail, setImageThumbnail] = useState(variant?.image)

  React.useEffect(() => {
    const img =
      variants.find(v => v.shopifyId === variant.shopifyId) || variants[0]
    setImageThumbnail(img?.image)
  }, [variant, variants, setImageThumbnail])

  return (
    <ImageGalleryWrapper>
      <div className="main-image">
        <Image fluid={activeImageThumbnail?.localFile.childImageSharp.fluid} />
      </div>
      <div className="image-thumbnails">
        {variants.map(v => {
          return (
            <ImageThumbnailVariant
              key={v.id}
              variant={v}
              handleClick={handleClick}
              isActive={v.shopifyId === variant?.shopifyId}
            />
          )
        })}
      </div>
    </ImageGalleryWrapper>
  )
}
