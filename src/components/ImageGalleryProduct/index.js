import React, { useState } from "react"
import Image from "gatsby-image"
import { ImageGalleryWrapper } from "../ImageGalleryVariant/styles"
import ImageThumbnailProduct from "./ImageThumbnailProduct"

export const ImageGalleryProduct = ({ images }) => {
  const [activeImage, setActiveImage] = useState(null)

  React.useEffect(() => {
    setActiveImage(images[0])
  }, [setActiveImage, images])

  const handleClickImage = img => {
    setActiveImage(img)
  }

  return (
    <ImageGalleryWrapper>
      <>
        <div className="main-image">
          <Image fluid={activeImage?.localFile.childImageSharp.fluid} />
        </div>
        <div className="image-thumbnails">
          {images?.map(img => {
            return (
              <ImageThumbnailProduct
                key={img.id}
                image={img}
                handleClick={handleClickImage}
                isActive={img.id === activeImage?.id}
              />
            )
          })}
        </div>
      </>
    </ImageGalleryWrapper>
  )
}
