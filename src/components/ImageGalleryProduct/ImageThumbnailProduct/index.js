import React from "react"
import Image from "gatsby-image"
import { ImageThumbnailWrapper } from "../../ImageGalleryVariant/ImageThumbnailVariant/styles"

export default function ImageThumbnailProduct({
  isActive,
  handleClick,
  image,
}) {
  return (
    <ImageThumbnailWrapper
      onClick={() => {
        handleClick(image)
      }}
      isActive={isActive}
    >
      <Image fluid={image.localFile.childImageSharp.fluid} />
    </ImageThumbnailWrapper>
  )
}
