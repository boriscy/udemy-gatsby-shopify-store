import styled from "styled-components"

export const ProductQuantityAdderWrapper = styled.div`
  label {
    margin: 5px 0px;
    display: block;
  }
  input[type="number"] {
    border-right: none;
  }
  button {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    text-transform: uppercase;
    &:focus,
    &:hover {
      color: white;
      background-color: rgba(4, 120, 87);
    }
  }
`
