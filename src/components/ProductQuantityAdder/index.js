import React, { useState } from "react"
import { ProductQuantityAdderWrapper } from "./styles"
import CartContext from "context/CartContext"

export function ProductQuantityAdder({ variant, product }) {
  const [quantity, setQuantity] = useState(1)
  // Use conext
  const { updateLineItem } = React.useContext(CartContext)

  const handleQuantityChange = e => {
    setQuantity(+e.currentTarget.value)
  }

  const handleSubmit = e => {
    e.preventDefault()
    updateLineItem({
      variantId: variant.shopifyId,
      quantity,
      handle: product.handle,
    })
  }

  return (
    <ProductQuantityAdderWrapper>
      <label className="font-semibold text-gray-600" htmlFor="variant-quantity">
        Quantity:
      </label>
      <form onSubmit={handleSubmit}>
        <input type="hidden" name="variant_id" value={variant?.id} />

        <input
          id="variant-quantity"
          type="number"
          min="1"
          disabled={!variant?.availableForSale}
          step="1"
          value={quantity}
          onChange={handleQuantityChange}
          className="font-semibold text-gray-700"
        />
        <button
          className="btn btn-border-green text-gray-700"
          type="submit"
          disabled={!variant?.availableForSale}
        >
          Add to Cart
        </button>
      </form>
    </ProductQuantityAdderWrapper>
  )
}
