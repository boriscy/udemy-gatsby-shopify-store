import React, { useState } from "react"
import { FaSearch } from "react-icons/fa"
import styled from "styled-components"
import { navigate } from "@reach/router"
import queryString from "query-string"
import { useLocation } from "@reach/router"

// Styles
const SearchForm = styled.form`
  margin-left: auto;
  input {
    padding: 4px 10px;
    border: 1px solid #aaa;
  }
  button {
    border: 1px solid #aaa;
    padding: 6px;
    border-left: none;
    color: white;
  }
`

// Component
export function Search() {
  const { search } = useLocation()
  const qs = queryString.parse(search)
  const searchTerm = qs.s ? qs.s : ""
  const [searchText, setSearchText] = useState(searchTerm)

  const handleSubmit = e => {
    e.preventDefault()
    navigate(`/all-products?s=${encodeURIComponent(searchText)}`)
  }

  return (
    <SearchForm onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Search"
        value={searchText}
        onChange={e => setSearchText(e.target.value)}
      />
      <button type="submit" className="bg-blue-400 hover:bg-blue-500">
        <FaSearch />
      </button>
    </SearchForm>
  )
}
