import React from "react"
import { ProductsGridWrapper } from "./styles"
import { ProductTile } from "components/ProductTile"

export function ProductsGrid({ products }) {
  return (
    <div>
      {!products.length && (
        <h2 className="text-gray-400 text-center">No products found</h2>
      )}
      <ProductsGridWrapper>
        {products.map(product => {
          return <ProductTile key={product.shopifyId} product={product} />
        })}
      </ProductsGridWrapper>
    </div>
  )
}
