import React from "react"
import {
  Layout,
  SEO,
  HomepageCollectionsGrid,
  FeaturedProducts,
} from "components"
import ProductContext from "context/ProductContext"

const IndexPage = () => {
  const collection = React.useContext(ProductContext)

  return (
    <Layout>
      <SEO title="Home Page" description="We start here" />
      <HomepageCollectionsGrid
        collections={collection.collections.filter(
          coll => coll.title !== "Featured Hats"
        )}
      />
      {!!collection.collections.find(c => c.title === "Featured Hats") && (
        <FeaturedProducts />
      )}
    </Layout>
  )
}

export default IndexPage
