import React from "react"
import { Layout, Filters, ProductsGrid, SEO } from "components"
import ProductContext from "context/ProductContext"
import styled from "styled-components"
import queryString from "query-string"
import { useLocation } from "@reach/router"
//import Fuse from "fuse.js"

const Content = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;
  margin-top: 20px;
  grid-gap: 20px;
`

export default function AllProducts() {
  const { products, collections } = React.useContext(ProductContext)

  const collectionProductsMap = {}
  if (collections) {
    collections.forEach(coll => {
      collectionProductsMap[coll.shopifyId] = coll.products.map(
        p => p.shopifyId
      )
    })
  }
  // Query string
  const { search } = useLocation()
  const qs = queryString.parse(search)
  let collectionIds = qs.c?.split(",") || []
  const searchTerm = qs.s

  // Search with search term
  const getFilteredProductsSearchTerm = prods => {
    const regex = new RegExp(searchTerm, "i")
    return prods.filter(p => regex.test(p.title) || regex.test(p.description))
  }

  // Search with categories
  const getFilteredProductsCategories = () => {
    if (collectionIds.length <= 0) {
      return products
    }

    return [
      ...new Set(
        collectionIds
          .map(collId => {
            return products.filter(p =>
              collectionProductsMap[collId].includes(p.shopifyId)
            )
          })
          .flat()
      ),
    ]
  }

  let filteredProducts = getFilteredProductsCategories()
  if (!!searchTerm) {
    filteredProducts = getFilteredProductsSearchTerm(filteredProducts)
  }

  return (
    <Layout>
      <SEO title="All products" description="Al products" />
      <h4>{filteredProducts.length} products</h4>

      <Content>
        <Filters collections={collections} />
        <ProductsGrid products={filteredProducts} />
      </Content>
    </Layout>
  )
}
