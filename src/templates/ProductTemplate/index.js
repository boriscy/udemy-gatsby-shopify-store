/* eslint-disable jsx-a11y/no-onchange */
import React, { useState } from "react"
import { graphql } from "gatsby"
import {
  Layout,
  ImageGalleryVariant,
  ImageGalleryProduct,
  ProductQuantityAdder,
  SEO,
} from "components"
import { Grid, SelectWrapper, PriceWrapper, SelectPriceWrapper } from "./styles"
//import CartContext from "context/CartContext"
import { navigate, useLocation } from "@reach/router"
import queryString from "query-string"

export const query = graphql`
  query ProductQuery($shopifyId: String) {
    shopifyProduct(shopifyId: { eq: $shopifyId }) {
      ...ShopifyProductFields
      variants {
        id
        shopifyId
        price
        title
        compareAtPrice
        availableForSale
        image {
          localFile {
            childImageSharp {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
`

export default function ProductTemplate(props) {
  const { search, origin, pathname } = useLocation()
  const variantId = queryString.parse(search).variant

  const product = props.data.shopifyProduct

  const [variant, setVariant] = useState(
    product.variants.find(v => v.shopifyId === variantId) || product.variants[0]
  )

  React.useEffect(() => {
    setVariant(
      product.variants.find(v => v.shopifyId === variantId) ||
        product.variants[0]
    )
  }, [setVariant, product, variantId])

  const setVariantAndRoute = newVariant => {
    setVariant(newVariant)
    navigate(
      `${origin}${pathname}?variant=${encodeURIComponent(
        newVariant.shopifyId
      )}`,
      {
        replace: true,
      }
    )
  }

  const handleVariantChange = ev => {
    const newVariant = product?.variants.find(
      v => v.shopifyId === ev.target.value
    )
    setVariantAndRoute(newVariant)
  }

  const prevPage = () => {}

  return (
    <Layout>
      <SEO title={product.title} description={product.description} />

      {props.location.state && props.location.state.prevPath && (
        <button onClick={() => navigate(-1)} className="btn mb-5">
          Back to products
        </button>
      )}

      <Grid>
        <div>
          <h1>{product.title}</h1>
          <p>{product.description}</p>
          {product.availableForSale}
          {product?.availableForSale &&
            !!variant &&
            product?.variants.length > 1 && (
              <SelectPriceWrapper>
                <SelectWrapper>
                  <strong>Variant</strong>
                  <select
                    onChange={handleVariantChange}
                    value={variant?.shopifyId}
                  >
                    {product?.variants.map(v => {
                      return (
                        <option key={v.id} value={v.shopifyId}>
                          {v.title}
                        </option>
                      )
                    })}
                  </select>
                </SelectWrapper>
              </SelectPriceWrapper>
            )}
          {!!variant && (
            <>
              <PriceWrapper>
                <span>
                  {variant?.price} <span className="currency">BOB</span>
                </span>
              </PriceWrapper>
              <ProductQuantityAdder variant={variant} product={product} />
            </>
          )}
        </div>
        <div>
          {product?.variants.length > 1 ? (
            <ImageGalleryVariant
              variants={product?.variants || []}
              variant={variant}
              handleClick={setVariantAndRoute}
            />
          ) : (
            <ImageGalleryProduct images={product?.images} />
          )}
        </div>
      </Grid>
    </Layout>
  )
}
