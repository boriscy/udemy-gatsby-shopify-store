import styled from "styled-components"

export const Grid = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  grid-row-gap: 10px;

  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
    > div:first-child {
      order: 2;
    }
    > div:last-child {
      order: 1;
    }
  }
`

export const SelectWrapper = styled.section`
  margin-top: 15px;
  > strong {
    display: block;
    margin-bottom: 8px;
  }
  > select {
    > option {
      padding: 2px 10px;
    }
  }
`

export const PriceWrapper = styled.div`
  margin-top: 15px;
  font-weight: bold;
  font-size: 30px;
  display: flex;
  > span {
    align-self: flex-end;
  }
  .currency {
    font-weight: normal;
  }
`

export const SelectPriceWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`
