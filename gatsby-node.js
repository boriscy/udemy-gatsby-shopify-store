const path = require("path")

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"],
    },
  })
}

// Create custom pages with URL for each product
exports.createPages = async ({ graphql, actions }) => {
  // Gatsby uses redux in the back so import actions method createPage
  const { createPage } = actions

  const { data } = await graphql(`
    {
      allShopifyProduct {
        nodes {
          handle
          shopifyId
        }
      }
    }
  `)

  data.allShopifyProduct.nodes.forEach(node => {
    createPage({
      path: `products/${node.handle}`,
      context: {
        shopifyId: node.shopifyId,
      },
      component: path.resolve("./src/templates/ProductTemplate/index.js"),
    })
  })
}
